import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

Mobile.startApplication(RunConfiguration.getProjectDir() + '/Plugins/DemoAppV2.apk', true)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementText(findTestObject('Mobile/beranda-page/text-welcome'), 'Welcome,')

Mobile.tap(findTestObject('Mobile/beranda-page/tombol_login-here'), 0)

Mobile.tap(findTestObject('Mobile/login-page/text-register-now'), 0)

Mobile.verifyElementText(findTestObject('Mobile/register_page/text-lets_join_our_community'), 'Lets join our community!')

Mobile.tap(findTestObject('Mobile/register_page/field_tanggal_lahir'), 0)

Mobile.tap(findTestObject('Mobile/register_page/button_prev_tanggal'), 0)

Mobile.tap(findTestObject('Mobile/register_page/a_tanggal'), 0)

tahun = Mobile.getText(findTestObject('Mobile/register_page/a_tahun'), 0)

haridanbulan = Mobile.getText(findTestObject('Mobile/register_page/a_hari_bulan_tanggal'), 0)

bulan = (haridanbulan.split(' ')[1])

System.print((bulan + '****') + tahun)

String dataTanggal = tanggal

String dTahun = dataTanggal.split('-')[2]

String dBulan = dataTanggal.split('-')[1]

String dTanggal = dataTanggal.split('-')[0]

int dataPencarian = Integer.parseInt(dTahun) + 1

String pencarianTahun = String.valueOf(dataPencarian)

while (!(tahun.equals(pencarianTahun))) {
    tahunawal = Mobile.getText(findTestObject('Mobile/register_page/a_tahun'), 0)

    Mobile.tap(findTestObject('Mobile/register_page/a_tahun'), 0)

    int data = Integer.parseInt(tahunawal) - 1

    String dataBaru = String.valueOf(data)

    Mobile.tap(findTestObject('Mobile/register_page/a_tahun_pilihan', [('tahun') : dataBaru]), 0)

    tahun = Mobile.getText(findTestObject('Mobile/register_page/a_tahun'), 0)
}

while (!(bulan.equals(dBulan)) || !(tahun.equals(dTahun))) {
    Mobile.tap(findTestObject('Mobile/register_page/button_prev_tanggal'), 0)

    Mobile.tap(findTestObject('Mobile/register_page/a_tanggal'), 0)

    tahun = Mobile.getText(findTestObject('Mobile/register_page/a_tahun'), 0)

    haridanbulan = Mobile.getText(findTestObject('Mobile/register_page/a_hari_bulan_tanggal'), 0)

    bulan = (haridanbulan.split(' ')[1])
}

Mobile.tap(findTestObject('Mobile/register_page/a_tanggal_pilihan', [('tanggalpilihan') : dTanggal]), 0)

Mobile.tap(findTestObject('Mobile/register_page/text-oke-tanggal'), 0)

Mobile.delay(2)

Mobile.takeScreenshot(FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()

