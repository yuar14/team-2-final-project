import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

Mobile.startApplication(RunConfiguration.getProjectDir() + '/Plugins/DemoAppV2.apk', true)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementText(findTestObject('Mobile/beranda-page/text-welcome'), 'Welcome,')

Mobile.tap(findTestObject('Mobile/beranda-page/tombol_login-here'), 0)

Mobile.verifyElementText(findTestObject('Mobile/login-page/text-sign-in'), 'Sign In')

Mobile.setText(findTestObject('Mobile/login-page/field_email'), 'arieseffendy01@gmail.com', 0)

Mobile.setText(findTestObject('Mobile/login-page/field_password'), '12345678', 0)

Mobile.delay(2)

Mobile.takeScreenshot()

Mobile.tap(findTestObject('Mobile/login-page/buton_login'), 0)

Mobile.verifyElementVisible(findTestObject('Mobile/beranda-page/text-recommended_event'), 0)

Mobile.delay(5)

Mobile.takeScreenshot()

Mobile.tap(findTestObject('Mobile/beranda-page/buton_icon-profile'), 0)

Mobile.delay(5)

Mobile.takeScreenshot()

Mobile.verifyElementText(findTestObject('Mobile/profile-page/text-profile'), ' Profile ')

Mobile.tap(findTestObject('Mobile/profile-page/button-icon-gear'), 0)

Mobile.takeScreenshot()

Mobile.tap(findTestObject('Mobile/profile-page/button-logout'), 0)

Mobile.delay(2)

Mobile.verifyElementText(findTestObject('Mobile/profile-page/text-login_terlebih_dahulu'), 'Kamu tidak dapat mengakses halaman ini, yuk login terlebih dahulu')

Mobile.delay(2)

Mobile.takeScreenshot()

Mobile.closeApplication()

