import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.click(findTestObject('Web/Login/button_Masuk'))

WebUI.verifyElementPresent(findTestObject('Web/Login/text-masuk'), 0)

WebUI.setText(findTestObject('Web/Login/field_email'), 'tisnaldianto+10@gmail.com')

WebUI.setEncryptedText(findTestObject('Web/Login/field_Katasandi'), 'fMhQ7SVMnhlpGWnDFuIlog==')

WebUI.click(findTestObject('Web/Login/button_Login'))

WebUI.click(findTestObject('Object Repository/Web/Change Profile/icon_profile'))

WebUI.click(findTestObject('Object Repository/Web/Change Profile/button_MyAccount'))

WebUI.verifyElementText(findTestObject('Web/Change Profile/text_porfil'), 'Profil')

WebUI.click(findTestObject('Web/Buy Event/button-invoice'))

WebUI.verifyElementPresent(findTestObject('Web/Buy Event/text_Invoices'), 0)

WebUI.click(findTestObject('Web/Buy Event/button-invoicedetail'))

WebUI.click(findTestObject('Web/Buy Event/button-cancelinvoice'))

WebUI.click(findTestObject('Web/Buy Event/button-cancelinvoiceOK'))

WebUI.verifyElementPresent(findTestObject('Web/Buy Event/text-CancelbyUser'), 0)

WebUI.takeScreenshot()

