<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>TCAP017 - Update number with an empty field</name>
   <tag></tag>
   <elementGuidId>7308f7ef-e165-406d-9a4b-11cbba23f763</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZTVkM2JhZmZmODIzMDRlMWQ4NWMwNDgzY2M2YmY3MmQ2MzYxY2ZiNGJmOTlmNzU4ZmY2N2Y5YzY2MjUxMTFiNjQyYzFkMTkwMjFiMmZhZDEiLCJpYXQiOjE2ODM1MTkxMzQuMDAwODUsIm5iZiI6MTY4MzUxOTEzNC4wMDA4NTMsImV4cCI6MTcxNTE0MTUzMy45OTc4ODksInN1YiI6IjY0Iiwic2NvcGVzIjpbXX0.ItSIgOtIKnar9dUrhGl-74aao8hA3t0TFxfZbmkKTSLuJH_cGDvlZS5VfLI-noIY7bhigASpZ_haGETA-t6c79PI0Rhpf8ZOuJ6ZwiFf5vE6gGEIPv7Hu_fYfW4lhGOGQ-mv_3stUyx2jLgDrnN8INoJSsg5392KR789-gatibx3Nti1gTkBYSo4TRabFAIy4i3sMO56ZxNCuUGH9PQlnYUccgecnF3rB2rM-39LdSQAye-RjQ4nv7m8qZY8I1mQSCyIi5C35xztudTm0v5ZyRgUBAzL3DL_PuCQ5Bh-X1UXqX81IgKWIPmaKoVYFIhU6Zs2PTRUscoW-XdAd8DQo4f4F4OfEz8PLzeYuJljYbfeON_8sQz0eDQDkJYlLKm_9RwgrSyck-LbXteMjNW3xi9bfjIc6qFt3DoQK3RXg8C8INXjWJuIw-bbQJx7S2eId3OIPJBStiluyaWgCmtzb_YisPwmSSmDzXRHquM1Y0UrGfWKOZaveKKPUYEEoWvXh10jk1X_kYmZyIVnGzGLVm_5Us7qUGOYmk4ILrpPS0d3ygD-9kt_nywH4Q-2ZR2ty2cx1dZaB9dOuNlqHeaj-1rlwrYqc218EcZRCFPKQ9ctc9SLatioooHE36lmYmjeinHG-1YqDpZD0x0AxdIlg1WMG5sozj1jiDDWoaowKRk</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;Naldi&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;whatsapp&quot;,
      &quot;value&quot;: &quot;&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;birth_date&quot;,
      &quot;value&quot;: &quot;1998-07-02&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;photo&quot;,
      &quot;value&quot;: &quot;Plugins/Photo1.jpg&quot;,
      &quot;type&quot;: &quot;File&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;bio&quot;,
      &quot;value&quot;: &quot;QA Batch 6 Participant&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;position&quot;,
      &quot;value&quot;: &quot;QA Engineer&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>213f9c7a-61bb-40c6-ad97-9376fa0df24b</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Token</name>
      <type>Main</type>
      <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZTVkM2JhZmZmODIzMDRlMWQ4NWMwNDgzY2M2YmY3MmQ2MzYxY2ZiNGJmOTlmNzU4ZmY2N2Y5YzY2MjUxMTFiNjQyYzFkMTkwMjFiMmZhZDEiLCJpYXQiOjE2ODM1MTkxMzQuMDAwODUsIm5iZiI6MTY4MzUxOTEzNC4wMDA4NTMsImV4cCI6MTcxNTE0MTUzMy45OTc4ODksInN1YiI6IjY0Iiwic2NvcGVzIjpbXX0.ItSIgOtIKnar9dUrhGl-74aao8hA3t0TFxfZbmkKTSLuJH_cGDvlZS5VfLI-noIY7bhigASpZ_haGETA-t6c79PI0Rhpf8ZOuJ6ZwiFf5vE6gGEIPv7Hu_fYfW4lhGOGQ-mv_3stUyx2jLgDrnN8INoJSsg5392KR789-gatibx3Nti1gTkBYSo4TRabFAIy4i3sMO56ZxNCuUGH9PQlnYUccgecnF3rB2rM-39LdSQAye-RjQ4nv7m8qZY8I1mQSCyIi5C35xztudTm0v5ZyRgUBAzL3DL_PuCQ5Bh-X1UXqX81IgKWIPmaKoVYFIhU6Zs2PTRUscoW-XdAd8DQo4f4F4OfEz8PLzeYuJljYbfeON_8sQz0eDQDkJYlLKm_9RwgrSyck-LbXteMjNW3xi9bfjIc6qFt3DoQK3RXg8C8INXjWJuIw-bbQJx7S2eId3OIPJBStiluyaWgCmtzb_YisPwmSSmDzXRHquM1Y0UrGfWKOZaveKKPUYEEoWvXh10jk1X_kYmZyIVnGzGLVm_5Us7qUGOYmk4ILrpPS0d3ygD-9kt_nywH4Q-2ZR2ty2cx1dZaB9dOuNlqHeaj-1rlwrYqc218EcZRCFPKQ9ctc9SLatioooHE36lmYmjeinHG-1YqDpZD0x0AxdIlg1WMG5sozj1jiDDWoaowKRk</value>
      <webElementGuid>4c50bb96-56b6-40b1-8467-925c8bfeb460</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Accept</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>972484a3-c04e-4cde-bab3-6add624724a6</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Cookie</name>
      <type>Main</type>
      <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZTVkM2JhZmZmODIzMDRlMWQ4NWMwNDgzY2M2YmY3MmQ2MzYxY2ZiNGJmOTlmNzU4ZmY2N2Y5YzY2MjUxMTFiNjQyYzFkMTkwMjFiMmZhZDEiLCJpYXQiOjE2ODM1MTkxMzQuMDAwODUsIm5iZiI6MTY4MzUxOTEzNC4wMDA4NTMsImV4cCI6MTcxNTE0MTUzMy45OTc4ODksInN1YiI6IjY0Iiwic2NvcGVzIjpbXX0.ItSIgOtIKnar9dUrhGl-74aao8hA3t0TFxfZbmkKTSLuJH_cGDvlZS5VfLI-noIY7bhigASpZ_haGETA-t6c79PI0Rhpf8ZOuJ6ZwiFf5vE6gGEIPv7Hu_fYfW4lhGOGQ-mv_3stUyx2jLgDrnN8INoJSsg5392KR789-gatibx3Nti1gTkBYSo4TRabFAIy4i3sMO56ZxNCuUGH9PQlnYUccgecnF3rB2rM-39LdSQAye-RjQ4nv7m8qZY8I1mQSCyIi5C35xztudTm0v5ZyRgUBAzL3DL_PuCQ5Bh-X1UXqX81IgKWIPmaKoVYFIhU6Zs2PTRUscoW-XdAd8DQo4f4F4OfEz8PLzeYuJljYbfeON_8sQz0eDQDkJYlLKm_9RwgrSyck-LbXteMjNW3xi9bfjIc6qFt3DoQK3RXg8C8INXjWJuIw-bbQJx7S2eId3OIPJBStiluyaWgCmtzb_YisPwmSSmDzXRHquM1Y0UrGfWKOZaveKKPUYEEoWvXh10jk1X_kYmZyIVnGzGLVm_5Us7qUGOYmk4ILrpPS0d3ygD-9kt_nywH4Q-2ZR2ty2cx1dZaB9dOuNlqHeaj-1rlwrYqc218EcZRCFPKQ9ctc9SLatioooHE36lmYmjeinHG-1YqDpZD0x0AxdIlg1WMG5sozj1jiDDWoaowKRk</value>
      <webElementGuid>ad3abc83-56c2-4417-b8ec-bfb5db0dab5c</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZTVkM2JhZmZmODIzMDRlMWQ4NWMwNDgzY2M2YmY3MmQ2MzYxY2ZiNGJmOTlmNzU4ZmY2N2Y5YzY2MjUxMTFiNjQyYzFkMTkwMjFiMmZhZDEiLCJpYXQiOjE2ODM1MTkxMzQuMDAwODUsIm5iZiI6MTY4MzUxOTEzNC4wMDA4NTMsImV4cCI6MTcxNTE0MTUzMy45OTc4ODksInN1YiI6IjY0Iiwic2NvcGVzIjpbXX0.ItSIgOtIKnar9dUrhGl-74aao8hA3t0TFxfZbmkKTSLuJH_cGDvlZS5VfLI-noIY7bhigASpZ_haGETA-t6c79PI0Rhpf8ZOuJ6ZwiFf5vE6gGEIPv7Hu_fYfW4lhGOGQ-mv_3stUyx2jLgDrnN8INoJSsg5392KR789-gatibx3Nti1gTkBYSo4TRabFAIy4i3sMO56ZxNCuUGH9PQlnYUccgecnF3rB2rM-39LdSQAye-RjQ4nv7m8qZY8I1mQSCyIi5C35xztudTm0v5ZyRgUBAzL3DL_PuCQ5Bh-X1UXqX81IgKWIPmaKoVYFIhU6Zs2PTRUscoW-XdAd8DQo4f4F4OfEz8PLzeYuJljYbfeON_8sQz0eDQDkJYlLKm_9RwgrSyck-LbXteMjNW3xi9bfjIc6qFt3DoQK3RXg8C8INXjWJuIw-bbQJx7S2eId3OIPJBStiluyaWgCmtzb_YisPwmSSmDzXRHquM1Y0UrGfWKOZaveKKPUYEEoWvXh10jk1X_kYmZyIVnGzGLVm_5Us7qUGOYmk4ILrpPS0d3ygD-9kt_nywH4Q-2ZR2ty2cx1dZaB9dOuNlqHeaj-1rlwrYqc218EcZRCFPKQ9ctc9SLatioooHE36lmYmjeinHG-1YqDpZD0x0AxdIlg1WMG5sozj1jiDDWoaowKRk</value>
      <webElementGuid>73233be0-8909-41ca-86bd-2019e8497f77</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.0</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.online/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)


assertThat(response.getStatusCode()).isIn(Arrays.asList(200, 201, 202))</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
