<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Daftar</name>
   <tag></tag>
   <elementGuidId>2644eea6-9f6e-4e66-b391-63c1790254fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#buttonRegisterTrack</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonRegisterTrack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e369f6e4-5f3a-4514-8ed2-946ee999e24a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonRegisterTrack</value>
      <webElementGuid>09e50e27-80be-406c-83d3-393593d0ad54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>1246b568-3735-4bb4-b969-cf5095ce7d70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-block btn-primary</value>
      <webElementGuid>c91553bf-3562-42c4-94cf-dc459c77baaf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            Daftar
                                        </value>
      <webElementGuid>f712ebaa-9a1e-487a-996d-724b2324b877</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonRegisterTrack&quot;)</value>
      <webElementGuid>643a89e9-77e4-49ba-9ab7-7206521f0872</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonRegisterTrack']</value>
      <webElementGuid>961c95e8-38d8-4084-ae74-57a024017c34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='syarat dan ketentuan'])[1]/following::button[1]</value>
      <webElementGuid>9e777b83-dfa5-4954-8946-f5490070ef13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Konfirmasi kata sandi'])[1]/following::button[1]</value>
      <webElementGuid>ff42e1cc-f862-49fe-a368-4404d1aa06da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar']/parent::*</value>
      <webElementGuid>42d5eaa2-cf6c-4c72-990f-f2b8e9e268d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/button</value>
      <webElementGuid>9910acdb-084e-4ecc-8033-8ffe9eb2c92b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonRegisterTrack' and @type = 'submit' and (text() = '
                                            Daftar
                                        ' or . = '
                                            Daftar
                                        ')]</value>
      <webElementGuid>ff452d4a-59f6-4ca2-8c18-78726c349437</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
