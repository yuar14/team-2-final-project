<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text-events</name>
   <tag></tag>
   <elementGuidId>c7f69ad6-28eb-445d-8610-e8d40a5073f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h2.titleEvent</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='containerEvent']/div/div/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>250d5d8a-6e13-4890-a06a-96740f42b8d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>titleEvent</value>
      <webElementGuid>9ced9825-3de2-4f79-926a-9dacd0ac78fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Events</value>
      <webElementGuid>d10078ca-dac2-4341-84a6-f99bd7533f77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;containerEvent&quot;)/div[1]/div[@class=&quot;row containerDrop&quot;]/div[@class=&quot;col-md-8&quot;]/h2[@class=&quot;titleEvent&quot;]</value>
      <webElementGuid>61127ef9-58c3-4c9d-b36d-4cc517c8fc67</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='containerEvent']/div/div/div/h2</value>
      <webElementGuid>5b9772ea-7257-4575-9065-277adedaec6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING.ID Event'])[1]/following::h2[1]</value>
      <webElementGuid>96c20779-7ef7-4cb1-a744-09b916187fec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 3: Predict using Machine Learning'])[1]/preceding::h2[1]</value>
      <webElementGuid>1a3a05b1-56b6-4f81-a0e6-120d30a43e79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='With Ziyad Syauqi Fawwazi'])[1]/preceding::h2[1]</value>
      <webElementGuid>cb5ffe01-b3c5-427a-9f87-6f70efdfc3d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/h2</value>
      <webElementGuid>69eef253-d63c-4f7b-a1cd-07a5791fc80e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Events' or . = 'Events')]</value>
      <webElementGuid>b5190930-c177-4bc6-9309-630530f491d6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
