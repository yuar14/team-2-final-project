<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_day4</name>
   <tag></tag>
   <elementGuidId>2be4262a-e1a5-4627-928d-2b4683280aa2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='blockListEvent']/a/div/div)[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4a3303f4-3ea5-4564-9911-5892a8d60804</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>blockHeader</value>
      <webElementGuid>adb5021f-7e13-4a5e-958f-c9729a9a3a4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        </value>
      <webElementGuid>be63ca77-061e-4b18-9b11-ede2c9131529</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]/div[@class=&quot;blockHeader&quot;]</value>
      <webElementGuid>4ddc2522-902c-476a-9bcd-67d5f2ea25c7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='blockListEvent']/a/div/div)[3]</value>
      <webElementGuid>f959ad0c-804b-4888-af05-f5162e74320c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a/div/div</value>
      <webElementGuid>23184090-7eb3-462f-90f4-cd973fcec25a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        ' or . = '
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        ')]</value>
      <webElementGuid>b1f56b29-61ae-4f40-b2be-1cac2d0bb69e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
