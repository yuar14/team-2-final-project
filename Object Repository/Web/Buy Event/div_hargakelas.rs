<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_hargakelas</name>
   <tag></tag>
   <elementGuidId>0ccb30de-9bdd-441a-b829-b197eba0781d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>cfd2ff4f-1f1e-4d1e-8892-8669f6d0c435</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wm-event-options</value>
      <webElementGuid>23d1a417-bae4-4d41-9ce5-429e40fa6aaa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            18 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            190 Hari 0 Jam 53  Menit  40 Detik 
                            

                        
                        
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        


                    
                </value>
      <webElementGuid>00a59b87-d86d-4e3c-b10d-2107aa05a798</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[@class=&quot;wm-sticky&quot;]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/aside[@class=&quot;col-md-4&quot;]/div[@class=&quot;wm-event-options&quot;]</value>
      <webElementGuid>7d59ef72-84fb-4b03-97d2-156ffea1d836</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[1]/following::div[1]</value>
      <webElementGuid>ff567225-62d2-4042-941f-8b07d747c9e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ziyad Syauqi Fawwazi'])[1]/following::div[1]</value>
      <webElementGuid>875d0168-dae9-4261-b484-76f607e98f78</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside/div[2]</value>
      <webElementGuid>884ae7fd-c2c5-40cd-98b2-1e479a585990</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            18 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            190 Hari 0 Jam 53  Menit  40 Detik 
                            

                        
                        
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        


                    
                ' or . = '
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            18 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            190 Hari 0 Jam 53  Menit  40 Detik 
                            

                        
                        
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        


                    
                ')]</value>
      <webElementGuid>23c66d16-f0f0-4280-96f9-b83d746fe0d3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
