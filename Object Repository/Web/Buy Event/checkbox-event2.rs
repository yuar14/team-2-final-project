<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkbox-event2</name>
   <tag></tag>
   <elementGuidId>52bf3fd7-3446-45fc-958f-2a1e2f0a3ade</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@id='check'])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>4a9a8a9b-02b7-47da-b069-d00e1958c49f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>56c8ce75-0882-4697-8b25-9a02cdbf2ffd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>85000|129|1</value>
      <webElementGuid>4113ceb5-49bd-4386-987b-db5832583183</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>1c0119e2-de70-4d6e-a8a6-2dcebc35c85e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Day 3: Predict using Machine Learning</value>
      <webElementGuid>d680cd69-f7ee-4beb-9e31-06741a4d0bc3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data</name>
      <type>Main</type>
      <value>500000</value>
      <webElementGuid>dae24706-90f2-4865-9044-3aaa59b60b18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>check</value>
      <webElementGuid>6439a300-4e2e-4ee9-b077-c4d45dfa4700</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cartForm&quot;)/div[@class=&quot;col-md-8&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-1&quot;]/div[1]/input[@id=&quot;check&quot;]</value>
      <webElementGuid>a5ca27cb-c63f-4e15-a017-7626a5f2d894</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@id='check'])[2]</value>
      <webElementGuid>0c45a23f-5faf-4b17-aa00-42a6b9cc2576</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div/div[2]/div/div/input</value>
      <webElementGuid>4d39067c-4883-4205-ad76-db94497ddb9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/input</value>
      <webElementGuid>8127da9d-fbca-48bb-a405-ba1d107827c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @name = 'Day 3: Predict using Machine Learning' and @id = 'check']</value>
      <webElementGuid>5536614d-d204-4d96-90fc-f45f347aad45</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
